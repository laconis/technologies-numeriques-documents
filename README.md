# Les technologies de l'édition numérique sont-elles des documents comme les autres ?
Projet d'article pour la revue scientifique de l'Enssib, _Balisages_ ([appel à articles](https://www.enssib.fr/revue-balisages)).

Attention l'adresse de ce dépôt est susceptible d'être modifiée.

![](publication-numerique.jpg)
